CREATE TABLE mapimages (
  mapimageid int NOT NULL AUTO_INCREMENT,
  mapimagedescription varchar(255),
  filename varchar(255) NOT NULL,
  showimage boolean NOT NULL,
  date_added TIMESTAMP,
  added_by varchar(255) NOT NULL,
  PRIMARY KEY (mapimageid)
);

CREATE TABLE progressbarimages (
  progressbarimageid int NOT NULL AUTO_INCREMENT,
  progressbarimagedescription varchar(255),
  filename varchar(255) NOT NULL,
  showimage boolean NOT NULL,
  date_added TIMESTAMP,
  added_by varchar(255) NOT NULL,
  PRIMARY KEY (progressbarimageid)
);