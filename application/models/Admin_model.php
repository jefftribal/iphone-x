<?php
class Admin_model extends CI_Model {

    public function __construct() {

            $this->load->database();
    }

    public function add_user() {

        $data = array(
            'username' => $this->input->post('username'),
            'passwd' => md5($this->input->post('password')),
            'fullname' => $this->input->post('fullname'),
            'email' => $this->input->post('email'),
            'accesslevel' => $this->input->post('accesslevel'),
            'isactive' => TRUE
        );

        $this->db->insert('adminusers', $data);
        return $this->db->insert_id();
    }

    //update user
    public function update_user($inuserid) {

        $this->db->set('fullname',$this->input->post('fullname'));
        $this->db->set('email',$this->input->post('email'));

        if ($this->session->userdata('auth_level') == 1) {
            $this->db->set('accesslevel',$this->input->post('accesslevel'));
        }

        $this->db->where('userid',$inuserid);
        $this->db->update('adminusers');

    }

    //update user password
    public function update_passwd($inuserid) {
        $this->db->set('passwd',md5($this->input->post('password')));
        $this->db->where('userid',$inuserid);
        $this->db->update('adminusers');
    }

    //auth user
    public function get_user($username, $password) {
        $this->db->select('userid,username,fullname,accesslevel');
        $this->db->where('username', $username);
        $this->db->where('passwd', md5($password));
        $this->db->where('isactive', TRUE);
        $query = $this->db->get('adminusers');
        return $query->row();

    }

    //get user list
    public function get_user_list() {

        $this->db->select('userid,username,fullname,email,isactive,accesslevel');
        $query = $this->db->get('adminusers');

        return $query->result_array();

    }

    //disable user
    public function disable_user() {
        
    }

    //get user details
    public function get_user_details($inuserid) {

        $query = $this->db->get_where('adminusers', array('userid' => $inuserid));

        if ($query->num_rows() > 0) {
            // return TRUE;
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

}
?>