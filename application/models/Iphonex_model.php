<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iphonex_model extends CI_Model {

	public function __construct() {

	        $this->load->database();
	}

	// ----- progress var section -----
	public function save_progressbarimage($inuserid, $infilename) {

	    $data = array(
	        'added_by' => $inuserid,
	        'filename' => $infilename,
	        'showimage' => FALSE
	    );

	    $this->db->insert('progressbarimages', $data);
	    return $this->db->insert_id();
	}

	public function delete_progressbarimage($inprogressbarimageid) {
		return $this->db->delete('progressbarimages', array('progressbarimageid' => $inprogressbarimageid));
	}

	public function get_active_progressbarimage() {

		$this->db->select('filename');
		$this->db->order_by('date_added', 'desc');
		$query = $this->db->get_where('progressbarimages', array('showimage' => TRUE), 1);
        return $query->row();
	}

	public function get_all_progressbarimages() {

        //get all video
        $this->db->select('progressbarimageid,filename,showimage');
        $this->db->order_by('progressbarimageid', 'asc');
        $query = $this->db->get('progressbarimages');
        return $query->result_array();
	}

	public function toggle_progressimage_status($inprogressbarimageid = NULL) {

	    if ($inprogressbarimageid === NULL) {
	        return 'ERROR';
	    } else {
	        //Toggle value
	        $togglequery = 'UPDATE progressbarimages SET showimage = NOT showimage WHERE progressbarimageid = ' . $inprogressbarimageid;
	        $query = $this->db->query($togglequery);

	        //get new value
	        $query = $this->db->get_where('progressbarimages', array('progressbarimageid' => $inprogressbarimageid));

	        if ($query->num_rows() > 0) {
	            $result = $query->row();
	            return $result->showimage;
	        } else {
	            return 'ERROR';
	        }
	        
	    }
	}

	// ----- mapimage section -----
	public function save_mapimage($inuserid, $infilename) {

	    $data = array(
	        'added_by' => $inuserid,
	        'filename' => $infilename,
	        'showimage' => FALSE
	    );

	    $this->db->insert('mapimages', $data);
	    return $this->db->insert_id();
	}

	public function delete_mapimage($inmapimageid) {
		return $this->db->delete('mapimages', array('mapimageid' => $inmapimageid));
	}

	public function get_active_mapimage() {

		$this->db->select('filename');
		$this->db->order_by('date_added', 'desc');
		$query = $this->db->get_where('mapimages', array('showimage' => TRUE), 1);
        return $query->row();
	}

	public function get_all_mapimages() {

        //get all video
        $this->db->select('mapimageid,filename,showimage');
        $this->db->order_by('mapimageid', 'asc');
        $query = $this->db->get('mapimages');
        return $query->result_array();
	}

	public function toggle_mapimage_status($inmapimageid = NULL) {

	    if ($inmapimageid === NULL) {
	        return 'ERROR';
	    } else {
	        //Toggle value
	        $togglequery = 'UPDATE mapimages SET showimage = NOT showimage WHERE mapimageid = ' . $inmapimageid;
	        $query = $this->db->query($togglequery);

	        //get new value
	        $query = $this->db->get_where('mapimages', array('mapimageid' => $inmapimageid));

	        if ($query->num_rows() > 0) {
	            $result = $query->row();
	            return $result->showimage;
	        } else {
	            return 'ERROR';
	        }
	        
	    }
	}


}

?>