<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">
			<h1>Thank you</h1>
			<p><strong><?php echo $user[0]['username']; ?> (<?php echo $user[0]['fullname']; ?>)</strong> with email address <strong>(<?php echo $user[0]['email']; ?>)</strong> updated as <strong>
				<?php
					switch ($user[0]['accesslevel']) {
						case 1: echo "Super Admin";
						break;

						case 2: echo "Video Admin";
						break;

						default: echo "Monitor";
					};  ?>
			</strong>.</p>
			<?php echo anchor('/admin','Home','class="btn btn-primary"'); ?>
			<?php echo anchor('useradmin/edituser/' . $user[0]['userid'],'Edit user','class="btn btn-primary"'); ?>
			<?php echo anchor('useradmin/changepasswd/' . $user[0]['userid'],'Change password','class="btn btn-primary"'); ?>

			<?php
			if ($authlevel == 1) {
			?>
				<?php echo anchor('useradmin/adduser','Add user','class="btn btn-primary"'); ?>
				<?php echo anchor('useradmin/listusers','List users','class="btn btn-primary"'); ?>	
			<?php
			}
			?>
		</div>
	</div>
</div>