<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>iPhone X Admin Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- <link href="/css/sticky-footer.css" rel="stylesheet"> -->
		<!-- <link href="/modessfbapp/css/sticky-footer.css" rel="stylesheet"> -->
		<link href="/css/sticky-footer.css" rel="stylesheet">

		<link href='https://fonts.googleapis.com/css?family=Monoton|Open+Sans:400,300' rel='stylesheet' type='text/css'>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style>
			body {
			  /*background-image: url(images/random_grey_variations.png);*/
			  /*background-image: url(/images/darkpurple.jpg);*/
			  background-color: #551A8B;
			}

			#logintitle {
			  padding-top: 100px;
			  color: white;
			  text-align: center;
			}

			#logintitle h1 {
			  font-size: 100px;
			  font-family: 'Monoton', cursive;
			  line-height: 1;
			  padding: 0;
			  margin: 0;
			  /*font-family: 'Mystery Quest', cursive;*/
			  /*font-family: 'Cherry Swash', cursive;*/
			}

			#logintitle h2 {
			  font-family: 'Open Sans', sans-serif;
			  font-size: 18px;
			  padding: 0;
			  margin: 0;
			}

			#loginform {
			  /*text-align: center;*/
			  padding-top: 50px;
			  color: white;
			}

			#footer {
			  background-color: black;
			  color: white;
			  opacity: 0.3;
			  text-align: center;
			}

			#footer p {
			  padding-top: 20px;
			}

			@media all and (max-width: 768px) {
			  #logintitle {
			    padding-top: 10px;
			  }

			  #logintitle h1 {
			    font-size: 100px;
			  }

			  #loginform {
			    padding-top: 10px;
			  }
			} 
		</style>

	</head>
	<body>
		<div id="wrap">
		<div class="container">
		  <div class="row">
		    <div class="col-sm-12" id="logintitle">
		        <h1>iPhone X Admin</h1>
		        <!-- <h2>Ministry On-line Job Order</h2> -->
		    </div>
		  </div>

		  <div class="row">
		    <div class="col-sm-4 col-sm-offset-4 col-xs-12" id="loginform">
		      <h4>Log In</h4>
		      	<?php
		      		$form_attr = array('role' => 'form');
		      		echo form_open('login',$form_attr);
		      	?>
		        <div class="form-group">
		          <!-- <label for="emailaddr">Email Address</label> -->
		          <input type="text" name="username" class="form-control input-lg" placeholder="User name">
		        </div>
		        <div class="form-group">
		          <!-- <label for="userpass">Password</label> -->
		          <input type="password" name="password" class="form-control input-lg" placeholder="Password">
		        </div>
		        <button type="submit" name="login" value="Login" class="btn btn-primary btn-lg btn-block">Sign in</button>
		      <!-- </form> -->
		      <h4 class="text-center" style="color: red;"><?php if(isset($error)) { echo $error; } ?></h4>
		    </div>
		  </div>
		</div>
		</div>

		<div id="footer">
		  <div class="container">
		    <p>Copyright 2017 - Digital Services - Tribal Worldwide Philippines</p>
		  </div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	</body>
</html>