<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3"><h1>Add user</h1></div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">

			<?php
				$form_attr = array('role' => 'form','id' => 'useradd');
				echo form_open('useradmin/adduser',$form_attr);
			?>

			<!-- <form id="useradd" action="" role="form"> -->

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('username') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="username">User name <?php echo form_error('username','<span> - *','*</span>'); ?></label>
					<input id="username" class="form-control" type="text" name="username" value="<?php echo set_value('username'); ?>">
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('password') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="password">Password <?php echo form_error('password','<span> - *','*</span>'); ?></label>
					<input id="password" class="form-control" type="password" name="password">
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('passwordconf') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="passwordconf">Confirm Password <?php echo form_error('passwordconf','<span> - *','*</span>'); ?></label>
					<input id="passwordconf" class="form-control" type="password" name="passwordconf">
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('fullname') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="fullname">Full name <?php echo form_error('fullname','<span> - *','*</span>'); ?></label>
					<input id="fullname" class="form-control" type="text" name="fullname" value="<?php echo set_value('fullname'); ?>">
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('email') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="email">Email Address <?php echo form_error('email','<span> - *','*</span>'); ?></label>
					<input id="email" class="form-control" type="email" name="email" value="<?php echo set_value('email'); ?>">
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('accesslevel') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="accesslevel">Access Level</label>
					<select class="form-control" name="accesslevel" id="accesslevel">
						<option <?php echo set_select('accesslevel', '1'); ?> value="1">Super Admin</option>
						<option <?php echo set_select('accesslevel', '2'); ?> value="2">Video Admin</option>
						<option <?php echo set_select('accesslevel', '3', TRUE); ?> value="3">Monitor</option>
					</select>
				</fieldset>				

				<input name="submit" type="submit" class="btn btn-default" value="Submit">
				<button onclick="history.back(-1)" class="btn btn-primary">Back</button>
			</form>

		</div>
	</div>

</div>