<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>iPhone X</title>
    <!-- Bootstrap -->
  
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
 
</head>

<body>
    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <section id="content-1">
        <div class="container">
            <div class="row">
               <div class="col-xs-12">
                    <h1 class="text-center">
                        <span class="green-text">HOW FAR WILL YOU GO FOR X?</span></h1>
                    <h4 class="text-center">
                        <strong>The
                            <img src="images/smart-iPhoneX-logo.jpg" alt="">  is here and we know you want it! <br><br>
Smart is giving you a chance to win 10 iPhone X.
The question isâ€”how far will you go for X?
</strong>
                    </h4>
                    
                    <img src="images/smart-iPhoneX-phone1.jpg" alt="" class=" center-block img-responsive">
                </div>
                <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                    <h2 class="text-center">
                        <strong>
                            <span class="green-text">Hereâ€™s how you can join: <br>
Open to all Smart subscribers
</span>
                    </strong>
                </h2>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="center-block img-responsive" src="images/smart-iPhoneX-icon1.jpg" alt="Los Angeles">
                                <h3 class="green-text text-center">
                                   <strong>POST</strong>
                                </h3>
                                <p class="text-center">
                                   Post using the hashtag #SmartiPhoneX on  Facebook, Instagram or Twitter. Make sure your  post is public to make it count!

                                
                                </p>
                            </div>
                            <div class="item">
                                <img class="center-block img-responsive" src="images/smart-iPhoneX-icon2.jpg" alt="Chicago">
                                <h3 class="green-text text-center">
                                  <strong> ZOOM</strong>
                                </h3>
                                <p class="text-center">
                                   Weâ€™ve hidden the iPhone X in different   locations around Metro Manila. The more you     post, the more weâ€™ll reveal the locations where     you can get a chance to win a free iPhone X.

                                </p>
                            </div>
                            <div class="item">
                                <img class="center-block img-responsive" src="images/smart-iPhoneX-icon3.png" alt="New York">
                                <h3 class="green-text text-center">
                                  <strong>FIND & UNLOCK
</strong>
                                </h3>
                                <p class="text-center">
                                  Search theÂ groundsÂ for Mr. X when the location is revealed.Â Show him that your phone is powered by Smart along with your #SmartiPhoneX post.Â Once qualified, choose and unlock a box for a chance to take home a free iPhone X.


                                </p>
                            </div>
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-menu-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-menu-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button type="button" class="btn center-block" data-toggle="modal" data-target="#mechanics">Full Mechanics</button>
<p class="text-center" style="
    padding-top: 25px;
">Promo runs December 10, 2017. <br> Per DTI-FTEB Permit No. 15262 Series of 2017.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="content-clues">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img id="mapimage" src="<?php echo $mapimage; ?>" alt="Smart iPhone X clues image" class="center-block img-responsive">
                </div>
            </div>
        </div>
    </section>
    <section id="content-clues-bar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="text-center">
                        <strong>Keep on posting to reveal clues where the phone is.</strong>
                    </h3>
                    <img id="progressbarimage" class="img-responsive center-block" src="<?php echo $progressbarimage; ?>" alt="Smart iPhone X clues meter bar">
                </div>
            </div>
        </div>
    </section>
   <section id="social-media">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-2 text-center">
                    <a class="twitter-timeline" href="https://twitter.com/hashtag/SmartiPhoneX" data-widget-id="934962457889484800">#SmartiPhoneX Tweets</a>
                    <script>
                    ! function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0],
                            p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s);
                            js.id = id;
                            js.src = p + "://platform.twitter.com/widgets.js";
                            fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");
                    </script>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 text-center">
                    <div class="fb-page" data-href="https://www.facebook.com/SmartCommunications/" data-tabs="timeline" data-height="590" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/SmartCommunications/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/SmartCommunications/">Smart Communications, Inc.</a></blockquote></div>
                </div>
            </div>
        </div>
    </section>
   <section id="content-join">     
        <div class="container">
            <div style="position: relative;" class="row">
                <div class="col-xs-12 col-sm-7 col-md-6 col-lg-5 col-lg-push-1 join-textbox">
                    <h2>
                       <strong> Can't Join?</strong>
                    </h2>
                    <p>If you want to get the new iPhone X ahead of everyone, you can Order Now by clicking on the link below. You can also get familiar with the most powerful iPhone yet by clicking Learn More.</p>
                         <article>
                        <a role="button" target="_blank" href="https://smart.com.ph/Postpaid/whats-new/iphonex" class="btn">Learn more</a> &nbsp;
                        <a role="button" target="_blank" href="http://store.smart.com.ph/view/2006/" class="btn">Apply now</a>
                    </article>
                </div>
                <div class="iphone-last-image visible-xs visible-lg">
                    <img src="images/smart-iPhoneX-phone2.png" alt="" class="center-block img-responsive">
                </div>
            </div>
    </section>

    <!-- modal mechanics -->
        <div id="mechanics" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center"><strong><u>Smart iPhone X â€“ How Far Will You Go For  X Contest Mechanics</u></strong></h4>
                </div>
                <div class="modal-body">
                    <ul>
        <li> WHO CAN JOIN
            <ol>
                <li>The Smart iPhone X Challenge (hereinafter The Contest) is open to all SMART SUBSCRIBERS, age 18 and above at the time of The Contest (hereinafter Contestant). Participation to the Contest does not involve any purchase whatsoever, may it be a product or an entry ticket, or anything similar in nature. All Contestants will not be asked for any fee to join The Contest.</li>
                <li>To qualify, Contestants must present his/her handset with the Smart signal as proof of a working Smart mobile number and at least one original public post on any social media account (Facebook, Twitter or Instagram) with the hashtag #SmartiPhoneX.</li>
                <li>Entry to The Contest, including all its peripheral accessories (tickets, queueing stubs, entry forms, and the like) are not transferable. Transfer of entry from one individual to another will result to disqualification.</li>
                <li>SMART COMMUNICATIONS, INC. (hereinafter The Company) reserves the right to disqualify, disallow, bar, and all similar actions, any group and/or individuals that The Company deems unfit and/or ineligible to join The Contest.</li>
                <li>The Company will not be responsible for any injury, harm, and/or any physical damage that The Contestants may incur during The Contest regardless if it is self-inflicted or was inflicted by another Contestant and/or other individuals.</li>
                <li>All employees of SMART COMMUNICATIONS, INC., its subsidiaries, as well as all its participating agencies, suppliers, event partners, organizers, and all such entities that are involved with The Contest, in part or in whole, including but not limited to all friends, spousal relationships, acquaintances, relatives up to the third degree, and generally all relations in good standing with the above mentioned, are ineligible to join The Contest and is therefore not allowed to affect the outcome of The Contest that may be favorable to a specific individual and/or group.</li>
            </ol>
        </li>
        <li> HOW TO JOIN
            <ol>
     <li> The Contest, hosted by SMART COMMUNICATIONS, INC., is an online-to-offline product hunt for a chance to win one of the ten free units of the Apple iPhone X (hereinafter iPhone X, The Prize).</li>
     <li>  There will be ten (10) Prizes distributed among three initially secret locations and will be awarded to the winning Contestants.</li>
     <li>  The secret locations where the prizes are distributed will be revealed through a map housed on http://www.smart.com.ph/FindTheX (hereinafter The Microsite).</li>
     <li>  The Contest officially starts with The Microsite going live on December 10, 2017 (Sunday) at 12:01 PM.</li>
     <li>  Once the location is revealed in the Microsite, contestants must search the grounds for the guy wearing an X shirt (hereinafter Mr. X).</li>
     <li>  Once they found Mr. X, Contestants must present his/her mobile phone/handset with the Smart signal as proof of a working Smart mobile number and at least one original public post on any social media account (Facebook, Twitter or Instagram) with the hashtag #SmartiPhoneX to qualify.</li>
     <li>  At the turn of the Contestant, he/she would need to choose one (1) box out of the ten available boxes to unlock. Only one (1) box can be selected, and once he/she has visibly made the selection, The Contestant can no longer choose another box.</li>
     <li>  Per a group of 10 boxes, there is only available prize (iPhone X), giving each contestant a 10% chance of winning. Once one of the prizes has been claimed, the next prized will be included on the next batch of 10 boxes. This will repeat until all prizes are claimed.</li>
     <li>  After selecting a box, The Contestant has 1 minute to unlock the box using the four-digit code based on the question posted in Smart’s Twitter account.</li>
     <li>  Once The Contestant has failed to open the box, or has opened a box that does not contain The Prize, he/she will then be asked to wait at the end of the line for another chance to open the box should there be available prizes left.</li>
     <li>  Once successful, The Contestant will then be approached by a certified representative of DTI and SMART COMMUNICATIONS, INC. in order to award The Prize and to formalize the win.</li>
     <li>  Once all The Prizes have been awarded, the leg of The Contest for that venue ends.</li>
     <li>  The same set of MECHANICS will be applied to all locations.</li>
     <li>  The Contest ends when all Prizes have been awarded.</li>
     <li>  The Contestant can only win once.</li>

            </ol>
        </li>
    </ul>
    <p><strong>CONTROL MEASURES</strong></p>
    <ol>
        <li> The Contest is only open to all Smart subscribers. Contestant must show a handset/mobile phone with the Smart signal as proof of a working Smart mobile number to qualify. Pocket wifis and Smart billing statements will not be accepted as proof of subscription. The Contestant must be the Smart subscriber and must own the handset/mobile phone presented to qualify. Contestants cannot use someone elseâ€™s Smart subscription or handset/mobile phone to join the contest.</li>
        <li> Contestant must post at least one public post in any of the following social media accounts (Facebook, Twitter & Instagram) to qualify. Posts must be set to public. Private posts will not be considered for qualification. Contestant must use their own social media accounts to join.</li>
        <li> Smart reserves the right to disqualify or disallow contestants whose actions will compromise the security and orderliness of the event and other contestants.</li>
        <li> Contestants must be able to present both mobile phone/handset with Smart signal and at least one original public post on any social media account (Facebook, Twitter or Instagram) with the hashtag #SmartiPhoneX. Failure to satisfy both criteria will result to disqualification.</li>
        <li> Contestant must be present when the draw happens. His/her number will only be called three times. If the contestant is not present after 3 calls, he/she will automatically be disqualified.</li>
        <li> Contestant can only choose one (1) box. Once he touched the box, he/she can no longer change his choice. Changing their choice of box after theyâ€™ve touched another box will result to disqualification.</li>
        <li> Once the Contestant have selected a box, they have one minute to try to open the box by answering the question. They can try to guess the code as many times possible within their given one minute.</li>
        <li> Every after Contestantâ€™s turn, the boxes will be shuffled in order to reset the odds of The Contest. If the Contestant is successful to open the box with a free iPhone X, the said box will still be returned and be included to maintain the odds of the Contest equal for all Contestants.</li>
        <li> DTI representative will be present for the duration of the contest.</li>
    </ol>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
    function checkPosition() {
        if (window.matchMedia('(min-width: 768px)').matches) {
            $('#myCarousel').addClass('row').removeAttr('id');
            $('.item').addClass('col-sm-4').removeClass('item');


            $("ol.carousel-indicators, .carousel-control").hide();

        } else {
            $('.carousel').attr('id', 'myCarousel').removeClass('row');
            $('.carousel-inner div').addClass('item').removeClass('col-sm-4');
            $("ol.carousel-indicators, .carousel-control").show();

        }

    }

    $(window).on('resize',function() {
        checkPosition();
   }).trigger('resize');


    // device detection
    </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    

    <!-- image refresh -->
    <script>

    $(document).ready(function() {
        setInterval(function() {
            $.get('https://unlockthex.com/home/getactivemapimage', function(data) {
                // $('#mapimage').attr('src', data + '?' + Math.random());
                $('#mapimage').attr('src', data);
            });

            $.get('https://unlockthex.com/home/getactiveprogressbarimage', function(data) {
                // $('#mapimage').attr('src', data + '?' + Math.random());
                $('#progressbarimage').attr('src', data);
            });
        }, 5000);
    });

    </script>
</body>

</html>