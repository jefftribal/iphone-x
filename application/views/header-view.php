<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Admin page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- uploader CSS -->
		<link rel="stylesheet" href="/css/upload.css" rel="stylesheet" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style>
			body {
			  /*background-image: url(images/random_grey_variations.png);*/
			  /*background-image: url(/sessions/images/darkpurple.jpg);*/
			  /*background-color: #551A8B;*/

			  /*min-height: 2000px;*/
  			  padding-top: 70px;
			}

			.pantylist {
				background-color: #eee;
				padding: 8px;
				border-radius: 5px;
			}

		</style>

		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery.js"></script>
		
	</head>
	<body>

		<!-- Fixed navbar -->
	    <nav class="navbar navbar-default navbar-fixed-top">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <?php echo anchor('/admin','iPhone X Admin','class="navbar-brand"'); ?>
	        </div>

	        <div id="navbar" class="navbar-collapse collapse">
	        	<ul class="nav navbar-nav">
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Image Management<span class="caret"></span></a>
					  <ul class="dropdown-menu">
					    <li><?php echo anchor('/admin/','List Map Images'); ?></li>
					    <li><?php echo anchor('/admin/uploadmap','Upload Map Images'); ?></li>
					    <li><?php echo anchor('/admin/progressbar','List Progress Bar Images'); ?></li>
					    <li><?php echo anchor('/admin/uploadprogressbar','Upload Progress Bar Images'); ?></li>
					  </ul>
					</li>

					<?php
					if ($authlevel == 1) { 
					?>

					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User management<span class="caret"></span></a>
					  <ul class="dropdown-menu">
					    <li><?php echo anchor('/useradmin/adduser','Add'); ?></li>
					    <li><?php echo anchor('/useradmin/listusers','List'); ?></li>
					  </ul>
					</li>

				    <?php
					}
				    ?>
	        	</ul>

		        <ul class="nav navbar-nav navbar-right">
		        	<li class="dropdown">
		        	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo $username; ?> <span class="caret"></span></a>
		        	  <ul class="dropdown-menu">
		        	  	<li><?php echo anchor('/useradmin/edituser','My Profile'); ?></li>
		        	  	<li><?php echo anchor('/logout','Logout'); ?></li>
		        	  </ul>
		        	</li>
		        </ul>

	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>