<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3"><h1>Change password</h1></div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">

			<?php
				$form_attr = array('role' => 'form','id' => 'useradd');
				echo form_open('useradmin/changepasswd/' . $userid,$form_attr);
			?>

			<!-- <form id="useradd" action="" role="form"> -->

				<fieldset class="form-group">
					<label class="control-label" for="username">User name</label>
					<input id="username" class="form-control" type="text" name="username" value="<?php echo $username; ?>" disabled>
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('password') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="password">Password <?php echo form_error('password','<span> - *','*</span>'); ?></label>
					<input id="password" class="form-control" type="password" name="password">
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('passwordconf') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="passwordconf">Confirm Password <?php echo form_error('passwordconf','<span> - *','*</span>'); ?></label>
					<input id="passwordconf" class="form-control" type="password" name="passwordconf">
				</fieldset>

				<input name="submit" type="submit" class="btn btn-primary" value="Update">
				<?php echo anchor('useradmin/listusers','Cancel','class="btn btn-primary"'); ?>
				<button onclick="history.back(-1)" class="btn btn-primary">Back</button>
			</form>

		</div>
	</div>

</div>