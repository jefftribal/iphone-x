		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

		<script type="text/javascript" src="/js/dmuploader.min.js"></script>

		<!-- upload script -->

		<script type="text/javascript">
		  
		  //-- Some functions to work with our UI
		  function add_log(message)
		  {
		    var template = '[' + new Date().getTime() + '] - ' + message;
		    
		    // $('#debug').find('ul').prepend(template);
		    console.log(template);
		  }
		  
		  function add_file(id, file) {
		    
		    var template = '<p>' + file.name + '</p>' +
		                    '<div class="progress" id="uploadFile' + id  + '">' +
		                        '<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:0%;">0%' +
		                        '</div>' +
		                      '</div>';
		      
		      $('#fileList').prepend(template);
		  }
		  
		  function update_file_status(id, status, message)
		  {
		    $('#uploadFile' + id).find('span.status').html(message).addClass(status);
		  }
		  
		  function update_file_progress(id, percent)
		  {
		    $('#uploadFile' + id).find('div.progress-bar').width(percent);
		    $('#uploadFile' + id).find('div.progress-bar').text(percent);
		  }
		  
		  // Upload Plugin itself
		  $('#drag-and-drop-zone').dmUploader({
		    url: '<?php echo $uploadurl; ?>',
		    dataType: 'json',
		    allowedTypes: 'image/*',
		    fileName: 'userfile',
		    extFilter: 'jpg',
		    maxFiles: 1,
		    /*extFilter: 'jpg;png;gif',*/

		    onInit: function(){
		      add_log('Penguin initialized :)');
		    },
		    onBeforeUpload: function(id){
		      add_log('Starting the upload of #' + id);
		      // add_log('FB UID - ' + userfbuid);
		      // $('#drag-and-drop-zone').data('dmUploader').settings.extraData = { fbuid: userfbuid };
		      
		      update_file_status(id, 'uploading', 'Uploading...');
		    },
		    onNewFile: function(id, file){
		      add_log('New file added to queue #' + id);
		      
		      add_file(id, file);
		    },
		    onComplete: function(){
		      add_log('All pending tranfers finished');
		    },
		    onUploadProgress: function(id, percent){
		      var percentStr = percent + '%';

		      update_file_progress(id, percentStr);
		    },
		    onUploadSuccess: function(id, data){
		      add_log('Upload of file #' + id + ' completed');
		      
		      add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));

		      add_log('Filename: ' + data.mapimageurl);
		      
		      update_file_status(id, 'success', 'Upload Complete');
		      
		      update_file_progress(id, '100%');

		    },
		    onUploadError: function(id, message){
		      add_log('Failed to Upload file #' + id + ': ' + message);

		      $('#myModalLabel').text('File Upload Error');
		      $('#errorbody').html('<p>Upload failed!</p>');
		      $('#errormodal').modal('show');
		      
		      update_file_status(id, 'error', message);
		    },
		    onFileTypeError: function(file){
		      add_log('File \'' + file.name + '\' cannot be added: must be an image file.');

		      $('#myModalLabel').text('File Upload Error');
		      $('#errorbody').html('<p>File ' + file.name + ' cannot be added: must be an image file.</p>');
		      $('#errormodal').modal('show');
		      
		    },
		    onFileSizeError: function(file){
		      add_log('File \'' + file.name + '\' cannot be added: size excess limit');

		      $('#myModalLabel').text('File Upload Error');
		      $('#errorbody').html('<p>File \'' + file.name + '\' cannot be added: size excess limit.</p>');
		      $('#errormodal').modal('show');

		    },
		    onFileExtError: function(file){
		      add_log('File \'' + file.name + '\' Invalid file type.');

		      $('#myModalLabel').text('File Upload Error');
		      $('#errorbody').html('<p>File \'' + file.name + '\' Invalid file type.</p>');
		      $('#errormodal').modal('show');
		    },
		    onFallbackMode: function(message){
		      alert('Browser not supported(do something else here!): ' + message);
		    }
		  });
		</script>

	</body>
</html>