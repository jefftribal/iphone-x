<div class="container-fluid">
	<div class="row">
		<?php
		//list panties
		foreach ($images as $image): 

			// if (strlen($mapimage['name']) > 20) {
			// 	$shortpantyname = substr($panty['name'], 0, strrpos(substr($panty['name'], 0, 20), ' '));
			// } else {
			// 	$shortpantyname = $panty['name'];
			// }

			?>

		<div class="col-xs-3">
			<div id="<?php echo 'image-item-' . $image[$imageid]; ?>" class="imagelist">
				<div class="imagecontainer" style="position: relative;">
					<img class="img-responsive" src="https://unlockthex.com/thumbnails/thumbnail-<?php echo $image['filename']; ?>" alt="">
				</div>
				<p>
					<strong>Status:</strong> <span id="<?php echo 'status-data-' . $image[$imageid]; ?>"><?php if ($image['showimage'] == TRUE) { echo 'Active'; } else { echo 'Hidden'; } ?></span><br>
				</p>
				<div class="btn-group btn-group-justified">
					<div class="btn-group">
						<button id="<?php echo 'view-btn-num-' . $image[$imageid]; ?>" data-imageid='<?php echo $image[$imageid]; ?>' data-imagestatus='<?php echo $image['showimage']; ?>' data-imagename='<?php echo $image['filename']; ?>' class="btn btn-primary" data-toggle="modal" data-target="#imageModal">View</button>
					</div>
					<?php
					if ($authlevel <= 2) {
					?>
					<div class="btn-group">
						<button id="<?php echo 'btn-num-' . $image[$imageid]; ?>" class="btn btn-primary" onclick="changeImageStatus(<?php echo $image[$imageid]; ?>)"><?php if ($image['showimage'] == TRUE) { echo 'Hide'; } else { echo 'Show'; } ?></button>
					</div>
					<div class="btn-group">
						<button id="<?php echo 'del-btn-num-' . $image[$imageid]; ?>" class="btn btn-primary" onclick="deleteImage(<?php echo $image[$imageid]; ?>)">Delete</button>
					</div>
					<?php } ?>
				</div>
			</div>
			<br>
		</div>

		<?php endforeach; ?>

	</div>
</div>

<!-- Modal section -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <h4 class="modal-title" id="myModalLabel">Map Image view</h4>
		  </div>
		  <div class="modal-body">
		  	<div class="row">
		  		<div class="col-xs-12">
		  			<div style="position: relative; margin: 0 auto; width: 540px;">
						<img class="img-responsive" src="/images/gallery-canvasbg.png" id="modalimage" alt="">
					</div>
		  		</div>
		  	</div>

		  </div>
		  <div class="modal-footer">
		  	<button id="modalimagebtntogl" type="button" class="btn btn-primary">Hide</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	</div>
</div>

<script>
	
	function setModalImage(inimagename) {
		var imageurl = 'https://unlockthex.com/mapimages/' + inimagename;
		$('#modalimage').attr("src", imageurl);
	}

	function deleteImage(ID) {
		var myurl = '<?php echo $deleteurl; ?>' + ID;
		$.get(myurl, function (retval) {
			// alert(retval + ' - ' + ID);
			if (retval == '1') {
				$('#image-item-'+ID).remove();
				// $('#del-btn-num-'+ID).text('Deleted');
				// $('#status-data-'+ID).text('Active');
				//change play btn data
				// $('#view-btn-num-'+ID).attr('data-imagestatus','1');
			}
		});
	}

	function changeImageStatus(ID) {
		var myurl = '<?php echo $toggleurl; ?>' + ID;
		$.get(myurl, function (retval) {
			// alert(retval + ' - ' + ID);
			if (retval == '1') {
				$('#btn-num-'+ID).text('Hide');
				$('#status-data-'+ID).text('Active');
				//change play btn data
				$('#view-btn-num-'+ID).attr('data-imagestatus','1');
			} else if (retval == '0') {
				$('#'+'btn-num-'+ID).text('Show');
				$('#status-data-'+ID).text('Hidden');
				//change play btn data
				$('#view-btn-num-'+ID).attr('data-imagestatus','0');
			}
		});
	}

	function changeImageStatusOnModal(ID) {
		var myurl = '<?php echo $toggleurl; ?>' + ID;
		$.get(myurl, function (retval) {

			if (retval == '1') {

				$('#btn-num-'+ID).text('Hide');
				$('#status-data-'+ID).text('Active'); 							
				//change play btn data
				$('#view-btn-num-'+ID).attr('data-imagestatus','1');

				$('#myModalLabel').text('Status: Active');
				$('#modalimagebtntogl').text('Hide');

			} else if (retval == '0') {

				$('#'+'btn-num-'+ID).text('Show');
				$('#status-data-'+ID).text('Hidden');
				//change play btn data
				$('#view-btn-num-'+ID).attr('data-imagestatus','0');

				$('#myModalLabel').text('Status: Hidden');
				$('#modalimagebtntogl').text('Show');

			}
		});
	}

	$(document).ready(function () {
		$('#imageModal').on('show.bs.modal', function (event) {
			 
			 var button = $(event.relatedTarget);
			 var imagename = button.data('imagename');
			 var imagestat = 'Active';
			 var modalbtnstat = 'Hide';
			 var imageurl = 'https://unlockthex.com/mapimages/' + imagename;

			 //get map image status using jquert
			 if ($('#view-btn-num-'+button.data('imageid')).attr('data-imagestatus') == "0") {
			 	imagestat = 'Hidden';
			 	modalbtnstat = 'Show';
			 }

			 $('#myModalLabel').text('Status: ' + imagestat);
			 $('#modalimagebtntogl').text(modalbtnstat);
			 $('#modalimagebtntogl').attr('onclick','changeImageStatusOnModal(' + button.data('imageid') +')');
			 $('#modalimage').attr("src", imageurl);

		})

	})

</script>
