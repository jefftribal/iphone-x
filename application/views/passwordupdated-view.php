<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">
			<h1>Password Updated</h1>
			<?php echo anchor('/admin','Home','class="btn btn-primary"'); ?>

			<?php
			if ($authlevel == 1) {
			?>

			<?php echo anchor('useradmin/adduser','Add user','class="btn btn-primary"'); ?>
			<?php echo anchor('useradmin/listusers','List users','class="btn btn-primary"'); ?>

			<?php
			}
			?>

		</div>
	</div>
</div>