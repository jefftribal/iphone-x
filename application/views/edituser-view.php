<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3"><h1>Edit user</h1></div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">

			<?php
				$form_attr = array('role' => 'form','id' => 'useradd');
				echo form_open('useradmin/edituser/' . $user[0]['userid'],$form_attr);
			?>

			<!-- <form id="useradd" action="" role="form"> -->

				<fieldset class="form-group">
					<label class="control-label" for="username">User name</label>
					<input id="username" class="form-control" type="text" name="username" value="<?php echo $user[0]['username']; ?>" disabled>
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('fullname') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="fullname">Full name <?php echo form_error('fullname','<span> - *','*</span>'); ?></label>
					<input id="fullname" class="form-control" type="text" name="fullname" value="<?php if (set_value('submit') != NULL) { echo set_value('fullname'); } else { echo $user[0]['fullname']; } ?>">
				</fieldset>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('email') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="email">Email Address <?php echo form_error('email','<span> - *','*</span>'); ?></label>
					<input id="email" class="form-control" type="email" name="email" value="<?php if (set_value('submit') != NULL) { echo set_value('email'); } else { echo $user[0]['email']; } ?>">
				</fieldset>

				<?php

				if ($authlevel == 1) {

				?>

				<fieldset class="form-group<?php if (set_value('submit') != NULL) { if (form_error('accesslevel') == NULL) {echo ' has-success';} else {echo ' has-error';};}; ?>">
					<label class="control-label" for="accesslevel">Access Level</label>
					<select class="form-control" name="accesslevel" id="accesslevel">
						<option <?php if (set_value('submit') != NULL) { echo set_select('accesslevel', '1'); } else { if ($user[0]['accesslevel'] == 1) { echo 'selected="selected"'; }} ?> value="1">Super Admin</option>
						<option <?php if (set_value('submit') != NULL) { echo set_select('accesslevel', '2'); } else { if ($user[0]['accesslevel'] == 2) { echo 'selected="selected"'; }} ?> value="2">Video Admin</option>
						<option <?php if (set_value('submit') != NULL) { echo set_select('accesslevel', '3', TRUE); } else { if ($user[0]['accesslevel'] == 3) { echo 'selected="selected"'; }} ?> value="3">Monitor</option>
					</select>
				</fieldset>

				<?php
				}
				?>

				<input name="submit" type="submit" class="btn btn-primary" value="Save">
				<?php echo anchor('useradmin/changepasswd/' . $user[0]['userid'],'Change password','class="btn btn-primary"'); ?>
				<button onclick="history.back(-1)" class="btn btn-primary">Back</button>
			</form>

		</div>
	</div>

</div>