<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1>User list</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table id="adminusers" class="table table-hover">
				<thead>
					<tr>
						<th>Full name</th>
						<th>User name</th>
						<th>Email</th>
						<th>Is Active</th>
						<th>Access Level</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php

					foreach ($users as $user): ?>

					<tr>
						<td><?php echo $user['fullname']; ?></td>
						<td><?php echo $user['username']; ?></td>
						<td><?php echo $user['email']; ?></td>
						<td>
							<?php
							if ($user['isactive']) { ?>
								
								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>

							<?php } else { ?>

								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>

							<?php 
							}
							?>
							
						</td>
						<td>
							<?php
								switch ($user['accesslevel']) {
									case 1: echo "Super Admin";
									break;

									case 2: echo "Video Admin";
									break;

									default: echo "Monitor";
								};  ?>
						</td>
						<td>
							<?php echo anchor('useradmin/edituser/' . $user['userid'],'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>','class="btn btn-primary btn-sm"'); ?>
							<!-- <a href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> -->
							<!-- &nbsp;&nbsp; -->
							<!-- <a href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a> -->
						</td>
					</tr>

					<?php endforeach; ?>

					<!-- <tr>
						<td>Ian Ong</td>
						<td>fizo</td>
						<td>ian.ong@test.com</td>
						<td><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>
						<td>Super Admin</td>
						<td><a href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
					</tr>
					<tr>
						<td>Ian Ong</td>
						<td>fizo</td>
						<td>ian.ong@test.com</td>
						<td><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>
						<td>Super Admin</td>
						<td><a href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
					</tr> -->
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<?php echo anchor('useradmin/adduser','Add another user','class="btn btn-primary"'); ?>
	</div>
</div>