<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1><?php echo $uploadtitle; ?></h1>
		</div>
	</div>
	<div class="row">
	    <div id="drag-and-drop-zone" class="col-xs-12 col-sm-12 col-md-12 text-center uploader">
	        <img src="/images/upload.png" class="img-responsive center-block">
	        <h1>Drag your file here<br>
	                or
	        </h1>
	        <div class="browser">
	          <label>
	            <span>Click to open the file Browser</span>
	            <input type="file" name="files[]" class="btn btn-default" title='Click to add Files'>
	          </label>
	        </div>
	        <h4>
	                *.jpg file <br>
	                1MB max file size
	        </h4>
	    </div>
	</div>
	<div class="row">
	    <div class="col-xs-12">
	        <div id="fileList">
	          
	          <!-- Files will be places here -->
	          

	        </div>
	    </div>
	</div>
</div>

<!-- Error modal -->
<div class="modal fade" id="errormodal" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Image Upload Error</h4>
      </div>
      <div id="errorbody" class="modal-body">
        <p>Error!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>