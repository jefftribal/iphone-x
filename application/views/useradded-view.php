<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">
			<h1>Thank you</h1>
			<p><strong><?php echo $username; ?></strong> added as <strong>
				<?php
					switch ($accesslevel) {
						case 1: echo "Super Admin";
						break;

						case 2: echo "Video Admin";
						break;

						default: echo "Monitor";
					};  ?>
			</strong>.</p>
			<?php echo anchor('useradmin/adduser','Add another user','class="btn btn-primary"'); ?>
			<?php echo anchor('useradmin/edituser/'. $userid,'Edit user','class="btn btn-primary"'); ?>
			<?php echo anchor('useradmin/listusers','List user','class="btn btn-primary"'); ?>
		</div>
	</div>
</div>