<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->helper('url_helper');
	    $this->load->library('session');
	}

	public function index() {
		//load helper libs
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('admin_model');

		//check if form was submitted
		if ($this->input->post('login') == NULL) {
			$this->load->view('login-view');
		} else {

			//form rules
			$this->form_validation->set_rules('username','UserName','required');
			$this->form_validation->set_rules('password','Password','required');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('login-view');
			} else {
				//validate user
				$userdetails = $this->admin_model->get_user($this->input->post('username'),$this->input->post('password'));

				if (isset($userdetails)) {

					$sessiondata = array(
						'user_id' => $userdetails->userid,
						'username' => $userdetails->username,
						// 'username' => 'Test2',
						'auth_level' => $userdetails->accesslevel,
						'logged_in' => TRUE
					);

					$this->session->set_userdata($sessiondata);

					//redirect to admin page
					redirect('/admin');

				} else {
					//error
					$errordata['error'] = 'Invalid Username or Password!';
					$this->load->view('login-view',$errordata);
				}
			}
		}

	}

}

?>