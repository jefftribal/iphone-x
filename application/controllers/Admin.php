<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->helper('url_helper');
	    $this->load->library('session');
	    $this->load->model('iphonex_model');
	}

	public function index() {
		//check if session exist
		$userdetails = $this->session->userdata('logged_in');

		if (! $userdetails == TRUE) {
			//redirect to login
			redirect('/login');
		} else {
			//load video data			
			$imagedata['images'] = $this->iphonex_model->get_all_mapimages();
			$imagedata['authlevel'] = $this->session->userdata('auth_level');
			$imagedata['imageid'] = 'mapimageid';
			$imagedata['toggleurl'] = '/admin/togglemapimagestatus/';
			$imagedata['deleteurl'] = '/admin/deletemapimage/';

			//userdetails
			$sessiondata['username'] = $this->session->userdata('username');
			$sessiondata['authlevel'] = $this->session->userdata('auth_level');

			//show admin page
			$this->load->view('header-view.php',$sessiondata);
			$this->load->view('admin-view.php',$imagedata);
			$this->load->view('footer-view.php');
			// var_dump($videodata);
		}
		
	}

	public function uploadmap() {

		//check if session exist
		$userdetails['logged_in'] = $this->session->userdata('logged_in');
		$userdetails['authlevel'] = $this->session->userdata('auth_level');

		if (! $userdetails['logged_in'] == TRUE) {
			//redirect to login
			redirect('/login');
		} else if ($userdetails['authlevel'] != 1) {
			//redirect to login
			redirect('/admin');
		} else {

			//get session data
			$sessiondata['username'] = $this->session->userdata('username');
			$sessiondata['authlevel'] = $this->session->userdata('auth_level');

			$uploaddata['uploadtitle'] = "Map Image Upload";
			$uploaddata['uploadurl'] = 'https://unlockthex.com/admin/mapimageupload';

			//show user list
			$this->load->view('header-view.php',$sessiondata);
			$this->load->view('upload-view.php',$uploaddata);
			$this->load->view('upload-footer-view.php',$uploaddata);

		}

	}

	public function mapimageupload() {

		//check if session exist
		$userdetails['logged_in'] = $this->session->userdata('logged_in');
		$userdetails['authlevel'] = $this->session->userdata('auth_level');

		if (! $userdetails['logged_in'] == TRUE) {
			//redirect to login
			header("HTTP/1.0 403 Forbidden");
            echo '{"error":"Access denied!"}';
		} else if ($userdetails['authlevel'] != 1) {
			//redirect to login
			header("HTTP/1.0 403 Forbidden");
            echo '{"error":"Access denied!"}';
		} else {

			//get session data
			$sessionuser = $this->session->userdata('username');

	        //config
	        $tempfilename = uniqid('mapimage-');

	        $uploadurl = 'https://unlockthex.com/mapimages/';

	        // $workingdir = "/var/www/html/beta/severn/";
	        $workingdir = "/var/www/unlockthex/";

	        //set timezone
	        date_default_timezone_set('Asia/Manila');

	        //Load libraries
	        // $this->load->library('form_validation');
	        // $this->load->library('email');

	        $config['upload_path']          = './mapimages/';
	        $config['allowed_types']        = 'jpg|JPG|jpeg|JPEG';
	        $config['max_size']             = 1024;
	        $config['file_name']            = $tempfilename;
	        $this->load->library('upload', $config);

	        //validate input
	        // $this->form_validation->set_rules('fbuid','FBUserID','required');

            if ( ! $this->upload->do_upload('userfile')) {

                $error = array('error' => $this->upload->display_errors());

                // $this->load->view('uploadvideo', $error);
                header("HTTP/1.0 400 Bad Request");
                // header("HTTP/1.0 405 Method Not Allowed");
                echo '{"error":"' . $error['error'] . '"}';

             } else {

                $data = array('upload_data' => $this->upload->data());

                //create thumbnail
                $execcommand = "convert " . $workingdir . "mapimages/" . $data['upload_data']['file_name'] . " -resize 350x350 " . $workingdir . "thumbnails/thumbnail-" . $data['upload_data']['file_name'];
                exec($execcommand);

                //save video info
                $mapimageid = $this->iphonex_model->save_mapimage($sessionuser, $data['upload_data']['file_name']);

                // echo '{"test":"12345"}';
                echo '{
                        "mapimageid":"' . $mapimageid . '",
                        "mapimageurl":"' . $uploadurl . $data['upload_data']['file_name'] . '"
                    }';

                //generate thumbnail                

                //convert size
                // $thumbnailresize = '/usr/bin/mogrify -resize 600x315 -background black -gravity center -extent 600x315 -format jpg -quality 70 ' . $workingdir . 'thumbnails/' . $config['file_name'] . '.jpg';
                // $thumbnailresize = '/usr/local/bin/mogrify -resize 600x315 -background black -gravity center -extent 600x315 -format jpg -quality 70 ' . $workingdir . 'thumbnails/' . $config['file_name'] . '.jpg';

                // exec($thumbnailresize);

            }
	                    
	    }
	}

	public function togglemapimagestatus($inmapimageid = NULL) {

		// //check if session exist
		// $userdetails = $this->session->userdata('logged_in');

		//check access level
		$authlevel = $this->session->userdata('auth_level');

		if ($authlevel <= 2) {

			if ($inmapimageid === NULL) {
				echo "ERROR";
			} else {
				//toggle video
				$gotoggle = $this->iphonex_model->toggle_mapimage_status($inmapimageid);
				echo $gotoggle;
			}

		} else {

			header($_SERVER["SERVER_PROTOCOL"] . ' 401 Unauthorized', true, 401);

		}

	}

	public function deletemapimage($inmapimageid = NULL) {

		// //check if session exist
		// $userdetails = $this->session->userdata('logged_in');

		//check access level
		$authlevel = $this->session->userdata('auth_level');

		if ($authlevel <= 2) {

			if ($inmapimageid === NULL) {
				echo "ERROR";
			} else {
				//toggle video
				$godelete = $this->iphonex_model->delete_mapimage($inmapimageid);
				echo $godelete;
			}

		} else {

			header($_SERVER["SERVER_PROTOCOL"] . ' 401 Unauthorized', true, 401);

		}

	}

	// ----- Progress Bar Section ----

	public function progressbar() {
		//check if session exist
		$userdetails = $this->session->userdata('logged_in');

		if (! $userdetails == TRUE) {
			//redirect to login
			redirect('/login');
		} else {

			//load video data			
			$imagedata['images'] = $this->iphonex_model->get_all_progressbarimages();
			$imagedata['authlevel'] = $this->session->userdata('auth_level');
			$imagedata['imageid'] = 'progressbarimageid';
			$imagedata['toggleurl'] = '/admin/toggleprogressbarimagestatus/';
			$imagedata['deleteurl'] = '/admin/deleteprogressbarimage/';

			//userdetails
			$sessiondata['username'] = $this->session->userdata('username');
			$sessiondata['authlevel'] = $this->session->userdata('auth_level');

			//show admin page
			$this->load->view('header-view.php',$sessiondata);
			$this->load->view('admin-view.php',$imagedata);
			$this->load->view('footer-view.php');
			// var_dump($videodata);
		}
		
	}

	public function uploadprogressbar() {

		//check if session exist
		$userdetails['logged_in'] = $this->session->userdata('logged_in');
		$userdetails['authlevel'] = $this->session->userdata('auth_level');

		if (! $userdetails['logged_in'] == TRUE) {
			//redirect to login
			redirect('/login');
		} else if ($userdetails['authlevel'] != 1) {
			//redirect to login
			redirect('/admin');
		} else {

			//get session data
			$sessiondata['username'] = $this->session->userdata('username');
			$sessiondata['authlevel'] = $this->session->userdata('auth_level');

			$uploaddata['uploadtitle'] = "Progress Bar Image Upload";
			$uploaddata['uploadurl'] = 'https://unlockthex.com/admin/progressbarimageupload';

			//show user list
			$this->load->view('header-view.php',$sessiondata);
			$this->load->view('upload-view.php',$uploaddata);
			$this->load->view('upload-footer-view.php',$uploaddata);

		}

	}

		public function progressbarimageupload() {

			//check if session exist
			$userdetails['logged_in'] = $this->session->userdata('logged_in');
			$userdetails['authlevel'] = $this->session->userdata('auth_level');

			if (! $userdetails['logged_in'] == TRUE) {
				//redirect to login
				header("HTTP/1.0 403 Forbidden");
	            echo '{"error":"Access denied!"}';
			} else if ($userdetails['authlevel'] != 1) {
				//redirect to login
				header("HTTP/1.0 403 Forbidden");
	            echo '{"error":"Access denied!"}';
			} else {

				//get session data
				$sessionuser = $this->session->userdata('username');

		        //config
		        $tempfilename = uniqid('progressbarimage-');

		        $uploadurl = 'https://unlockthex.com/mapimages/';

		        // $workingdir = "/var/www/html/beta/severn/";
		        $workingdir = "/var/www/unlockthex/";

		        //set timezone
		        date_default_timezone_set('Asia/Manila');

		        //Load libraries
		        // $this->load->library('form_validation');
		        // $this->load->library('email');

		        $config['upload_path']          = './mapimages/';
		        $config['allowed_types']        = 'jpg|JPG|jpeg|JPEG';
		        $config['max_size']             = 1024;
		        $config['file_name']            = $tempfilename;
		        $this->load->library('upload', $config);

		        //validate input
	            if ( ! $this->upload->do_upload('userfile')) {

	                $error = array('error' => $this->upload->display_errors());

	                // $this->load->view('uploadvideo', $error);
	                header("HTTP/1.0 400 Bad Request");
	                // header("HTTP/1.0 405 Method Not Allowed");
	                echo '{"error":"' . $error['error'] . '"}';

	             } else {

	                $data = array('upload_data' => $this->upload->data());

	                //create thumbnail
	                $execcommand = "convert " . $workingdir . "mapimages/" . $data['upload_data']['file_name'] . " -resize 350x350 " . $workingdir . "thumbnails/thumbnail-" . $data['upload_data']['file_name'];
	                exec($execcommand);

	                //save image info
	                $mapimageid = $this->iphonex_model->save_progressbarimage($sessionuser, $data['upload_data']['file_name']);

	                // echo '{"test":"12345"}';
	                echo '{
	                        "progressbarimageid":"' . $mapimageid . '",
	                        "progressbarimageurl":"' . $uploadurl . $data['upload_data']['file_name'] . '"
	                    }';

	            }
		                    
		    }
		}

		public function toggleprogressbarimagestatus($inprogressbarimageid = NULL) {

			// //check if session exist
			// $userdetails = $this->session->userdata('logged_in');

			//check access level
			$authlevel = $this->session->userdata('auth_level');

			if ($authlevel <= 2) {

				if ($inprogressbarimageid === NULL) {
					echo "ERROR";
				} else {
					//toggle video
					$gotoggle = $this->iphonex_model->toggle_progressimage_status($inprogressbarimageid);
					echo $gotoggle;
				}

			} else {

				header($_SERVER["SERVER_PROTOCOL"] . ' 401 Unauthorized', true, 401);

			}

		}

		public function deleteprogressbarimage($inprogressbarimageid = NULL) {

			// //check if session exist
			// $userdetails = $this->session->userdata('logged_in');

			//check access level
			$authlevel = $this->session->userdata('auth_level');

			if ($authlevel <= 2) {

				if ($inprogressbarimageid === NULL) {
					echo "ERROR";
				} else {
					//toggle video
					$godelete = $this->iphonex_model->delete_progressbarimage($inprogressbarimageid);
					echo $godelete;
				}

			} else {

				header($_SERVER["SERVER_PROTOCOL"] . ' 401 Unauthorized', true, 401);

			}

		}

}

?>