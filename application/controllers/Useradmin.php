<?php
class Useradmin extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->helper('url_helper');
	    $this->load->library('session');
	    $this->load->model('admin_model');
	}

	// public function index() {
	// 	//check if session exist
	// 	$userdetails = $this->session->userdata('logged_in');

	// 	if (! $userdetails == TRUE) {
	// 		//redirect to login
	// 		redirect('/login');
	// 	} else {

	// 		//show user list
	// 		$this->load->view('header-view.php');
	// 		$this->load->view('admin-view.php');
	// 		$this->load->view('footer-view.php');
	// 		// var_dump($videodata);
	// 	}
		
	// }

	public function changepasswd($inuserid = NULL) {
		//load helper libs
		$this->load->helper('form');
		$this->load->library('form_validation');

		//check if session exist
		$userdetails['logged_in'] = $this->session->userdata('logged_in');
		$userdetails['authlevel'] = $this->session->userdata('auth_level');

		if (! $userdetails['logged_in'] == TRUE) {
			//redirect to login
			redirect('/login');

		} else {

			if ($inuserid == NULL) {
				$inuserid = $this->session->userdata('user_id');
			} else {
				if ($userdetails['authlevel'] != 1) {
					$inuserid = $this->session->userdata('user_id');
				}
			}

			//form rules
			$this->form_validation->set_rules('password','Password','required');
			$this->form_validation->set_rules('passwordconf','ConfirmPassword','required|matches[password]');

			if ($this->form_validation->run() == TRUE) {

				//save user data
				$userid = $this->admin_model->update_passwd($inuserid);

				//redirect to thank you page
				redirect('useradmin/passwordupdated/' . $inuserid);

			} else {

				//get userid details
				$userdata = $this->admin_model->get_user_details($inuserid);

				//userdetails
				$sessiondata['username'] = $this->session->userdata('username');
				$sessiondata['authlevel'] = $this->session->userdata('auth_level');

				//show add user form
				$this->load->view('header-view.php', $sessiondata);
				$this->load->view('changepasswd-view.php', $userdata[0]);
				$this->load->view('footer-view.php');
			}
			
		}

	}

	public function edituser($inuserid = NULL) {

		//load helper libs
		$this->load->helper('form');
		$this->load->library('form_validation');

		//check if session exist
		$userdetails['logged_in'] = $this->session->userdata('logged_in');
		$userdetails['authlevel'] = $this->session->userdata('auth_level');

		if (! $userdetails['logged_in'] == TRUE) {
			//redirect to login
			redirect('/login');
		} else {

			if ($inuserid == NULL) {
				$inuserid = $this->session->userdata('user_id');
			} else {
				if ($userdetails['authlevel'] != 1) {
					$inuserid = $this->session->userdata('user_id');
				}
			}

			//get userid details
			$userdata['user'] = $this->admin_model->get_user_details($inuserid);

			if ($userdata == NULL) {

				redirect('useradmin/listusers');
				// $userdata = $this->session->userdata('user_id');

			} else {

				//form rules
				$this->form_validation->set_rules('fullname','Fullename','required');

				if ($userdetails['authlevel'] == 1) {
					$this->form_validation->set_rules('accesslevel','Accesslevel','required');
				}

				//check if submit
				if ($this->input->post('submit') != NULL) {

					if ($this->input->post('email') != $userdata['user'][0]['email']) {
						$this->form_validation->set_rules('email','email','trim|required|valid_email|is_unique[adminusers.email]');	
					}

				}

				if ($this->form_validation->run() == TRUE) {

					//save user data
					$this->admin_model->update_user($inuserid);

					//redirect to thank you page
					redirect('useradmin/userupdated/' . $inuserid);

				} else {

					//get userid details
					// $userdata = $this->admin_model->get_user_details($inuserid);

					//userdetails
					$sessiondata['username'] = $this->session->userdata('username');
					$sessiondata['authlevel'] = $this->session->userdata('auth_level');

					$userdata['authlevel'] = $this->session->userdata('auth_level');

					if ($userdata) {
						$this->load->view('header-view.php', $sessiondata);
						$this->load->view('edituser-view.php', $userdata);
						$this->load->view('footer-view.php');
					}

				}

			}
		}		
	}

	public function listusers() {

		//check if session exist
		$userdetails['logged_in'] = $this->session->userdata('logged_in');
		$userdetails['authlevel'] = $this->session->userdata('auth_level');

		if (! $userdetails['logged_in'] == TRUE) {
			//redirect to login
			redirect('/login');
		} else if ($userdetails['authlevel'] != 1) {
			//redirect to login
			redirect('/admin');
		} else {

			//get video list
			$userdata['users'] = $this->admin_model->get_user_list();

			//get session data
			$sessiondata['username'] = $this->session->userdata('username');
			$sessiondata['authlevel'] = $this->session->userdata('auth_level');

			//show user list
			$this->load->view('header-view.php',$sessiondata);
			$this->load->view('listusers-view.php', $userdata);
			$this->load->view('footer-view.php');

			// var_dump($videodata);
		}

	}

	public function adduser() {	
		//load helper libs
		$this->load->helper('form');
		$this->load->library('form_validation');

		//check if session exist
		$userdetails['logged_in'] = $this->session->userdata('logged_in');
		$userdetails['authlevel'] = $this->session->userdata('auth_level');

		if (! $userdetails['logged_in'] == TRUE) {

			//redirect to login
			redirect('/login');

		} else if ($userdetails['authlevel'] != 1) {

			//redirect to login
			redirect('/admin');

		} else {

			//form rules
			$this->form_validation->set_rules('username','UserName','trim|required|is_unique[adminusers.username]|min_length[3]|max_length[20]');
			$this->form_validation->set_rules('password','Password','required');
			$this->form_validation->set_rules('passwordconf','ConfirmPassword','required|matches[password]');
			$this->form_validation->set_rules('fullname','Fullename','required');
			$this->form_validation->set_rules('email','email','trim|required|valid_email|is_unique[adminusers.email]');
			$this->form_validation->set_rules('accesslevel','Accesslevel','required');

			if ($this->form_validation->run() == TRUE) {

				//save user data
				$userid = $this->admin_model->add_user();

				//redirect to thank you page
				redirect('useradmin/useradded/' . $userid);

			} else {

				//get session data
				$sessiondata['username'] = $this->session->userdata('username');
				$sessiondata['authlevel'] = $this->session->userdata('auth_level');

				//show add user form
				$this->load->view('header-view.php',$sessiondata);
				$this->load->view('adduser-view.php');
				$this->load->view('footer-view.php');
			}
			
		}

	}

	public function passwordupdated($userid = NULL) {

		if ($userid == NULL) {
			redirect('admin');
		} else {

			//get userid details
			$userdata['user'] = $this->admin_model->get_user_details($userid);

			if ($userdata) {

				//get session data
				$sessiondata['username'] = $this->session->userdata('username');
				$sessiondata['authlevel'] = $this->session->userdata('auth_level');

				$userdata['authlevel'] = $this->session->userdata('auth_level');

				$this->load->view('header-view.php', $sessiondata);
				$this->load->view('passwordupdated-view.php', $userdata);
				// var_dump($userdata);
				$this->load->view('footer-view.php');
			}
		}
	}

	public function useradded($userid = NULL) {

		if ($userid == NULL) {
			redirect('admin');
		} else {

			//get userid details
			$userdata = $this->admin_model->get_user_details($userid);

			if ($userdata) {

				//get session data
				$sessiondata['username'] = $this->session->userdata('username');
				$sessiondata['authlevel'] = $this->session->userdata('auth_level');

				$this->load->view('header-view.php', $sessiondata);
				$this->load->view('useradded-view.php', $userdata[0]);
				// var_dump($userdata);
				$this->load->view('footer-view.php');
			}
		}
	}

	public function userupdated($userid = NULL) {

		if ($userid == NULL) {
			redirect('admin');
		} else {

			//get userid details
			$userdata['user'] = $this->admin_model->get_user_details($userid);

			if ($userdata) {

				//get session data
				$sessiondata['username'] = $this->session->userdata('username');
				$sessiondata['authlevel'] = $this->session->userdata('auth_level');

				$userdata['authlevel'] = $this->session->userdata('auth_level');

				$this->load->view('header-view.php', $sessiondata);
				$this->load->view('userupdated-view.php', $userdata);
				// var_dump($userdata);
				$this->load->view('footer-view.php');
			}
		}
	}

}