<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->helper('url_helper');
		$this->load->model('iphonex_model');
	}

	public function index() {

		//Get active mapimage

		$activemapimage = $this->iphonex_model->get_active_mapimage();
		$activeprogressbarimage = $this->iphonex_model->get_active_progressbarimage();

		if ($activemapimage) {
			$activeimagedata["mapimage"] = '/mapimages/' . $activemapimage->filename;
		} else {
			$activeimagedata["mapimage"] = NULL;
		}

		if ($activeprogressbarimage) {
			$activeimagedata["progressbarimage"] = '/mapimages/' . $activeprogressbarimage->filename;
		} else {
			$activeimagedata["progressbarimage"] = NULL;
		}

		$this->load->view('home-view', $activeimagedata);
	}

	public function getactivemapimage() {

		$activemapimage = $this->iphonex_model->get_active_mapimage();

		if ($activemapimage) {
			echo 'https://unlockthex.com/mapimages/' . $activemapimage->filename;
		} else {
			echo "";
		}
	}

	public function getactiveprogressbarimage() {

		$activeprogressbarimage = $this->iphonex_model->get_active_progressbarimage();

		if ($activeprogressbarimage) {
			echo 'https://unlockthex.com/mapimages/' . $activeprogressbarimage->filename;
		} else {
			echo "";
		}
	}

}